package com.bruce.intellijplugin.generategetters.complexreturntype;

import com.bruce.intellijplugin.generategetters.ParamInfo;
import com.bruce.intellijplugin.generategetters.utils.PsiToolUtils;
import com.intellij.psi.PsiParameter;
import org.jetbrains.annotations.NotNull;

/**
 * @Author bruce.ge
 * @Date 2017/1/28
 * @Description
 */
public class MapReturnTypeHandler implements ComplexReturnTypeHandler {

    @NotNull
    @Override
    public InsertDto handle(@NotNull ParamInfo returnParamInfo, String splitText, PsiParameter[] parameters) {
        InsertDto insertDto = new InsertDto();
        String returnVariableName = "";
        StringBuilder insertText = new StringBuilder();
        insertText.append(splitText);
        if (returnParamInfo.getParams().size() > 1) {
            String firstParamRealName = returnParamInfo.getParams().get(0).getRealName();
            String secondParamRealName = returnParamInfo.getParams().get(1).getRealName();
            returnVariableName = PsiToolUtils.lowerStart(firstParamRealName) + "Map";
            insertText.append("Map<").append(firstParamRealName).append(",").append(secondParamRealName)
                    .append("> " + returnVariableName).append("=");
        } else {
            returnVariableName = "map";
            insertText.append("Map " + returnVariableName + "=");
        }

            insertText.append("new HashMap<>();");

        insertText.append(splitText + "return " + returnVariableName + ";");

        insertDto.setAddedText(insertText.toString());
        return insertDto;
    }
}
