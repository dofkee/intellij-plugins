package com.bruce.intellijplugin.generategetters.utils;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.psi.PsiDocumentManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by bruce.ge on 2016/12/23.
 */
public class PsiDocumentUtils {
    public static void commitAndSaveDocument(@NotNull PsiDocumentManager psiDocumentManager, @Nullable Document document) {
        if (document != null) {
            psiDocumentManager.doPostponedOperationsAndUnblockDocument(document);
            psiDocumentManager.commitDocument(document);
            FileDocumentManager.getInstance().saveDocument(document);
        }
    }
}
