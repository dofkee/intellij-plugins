package com.bruce.intellijplugin.generategetters;

import com.bruce.intellijplugin.generategetters.complexreturntype.*;
import com.bruce.intellijplugin.generategetters.utils.PsiClassUtils;
import com.bruce.intellijplugin.generategetters.utils.PsiDocumentUtils;
import com.bruce.intellijplugin.generategetters.utils.PsiToolUtils;
import com.google.common.collect.Maps;
import com.intellij.codeInsight.intention.PsiElementBaseIntentionAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.util.PsiTypesUtil;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;

import java.util.*;

/**
 * Created by bruce.ge on 2016/12/23.
 */
public class GenerateAllGettersAction extends PsiElementBaseIntentionAction {

    public static final String GENERATE_SETTER_METHOD = "generate all getters";
    public static final String IS = "is";
    public static final String GET = "get";
    
    @NotNull
    private static Map<String, ComplexReturnTypeHandler> handlerMap = new HashMap<String, ComplexReturnTypeHandler>() {{
        put("java.util.List", new ListReturnTypeHandler());
        put("java.util.Set", new SetReturnTypeHandler());
        put("java.util.Map", new MapReturnTypeHandler());
    }};

    @Override
    public void invoke(@NotNull Project project, Editor editor, @NotNull PsiElement element) throws IncorrectOperationException {
        PsiElement psiParent = PsiTreeUtil.getParentOfType(element, PsiLocalVariable.class, PsiMethod.class);
        
        if (psiParent == null) {
            return;
        }
        if (psiParent instanceof PsiLocalVariable) {
            PsiLocalVariable psiLocal = (PsiLocalVariable) psiParent;
            if (!(psiLocal.getParent() instanceof PsiDeclarationStatement)) {
                return;
            }
            handleWithLocalVariable(psiLocal, project, psiLocal);

        } else if (psiParent instanceof PsiMethod) {
            PsiMethod method = (PsiMethod) psiParent;
            if (method.getReturnType() == null) {
                return;
            }

            handleWithMethod(method, project, method);
        }
    }

    private void handleWithMethod(@NotNull PsiMethod method, @NotNull Project project, @NotNull PsiElement element) {
        PsiDocumentManager psiDocumentManager = PsiDocumentManager.getInstance(project);
        Document document = psiDocumentManager.getDocument(element.getContainingFile());
        String splitText = extractSplitText(method, document);
        ParamInfo returnTypeInfo = PsiToolUtils.extractParamInfo(method.getReturnType());
        InsertDto dto = null;
        if (returnTypeInfo.getCollectPackege() != null && handlerMap.containsKey(returnTypeInfo.getCollectPackege())) {
            dto = handlerMap.get(returnTypeInfo.getCollectPackege()).handle(returnTypeInfo, splitText, method.getParameterList().getParameters());
        } else {
            PsiClass returnTypeClass = PsiTypesUtil.getPsiClass(method.getReturnType());
            dto = getBaseInsertDto(splitText, method.getParameterList().getParameters(), returnTypeClass);
        }
        if (dto.getAddedText() != null) {
            document.insertString(method.getBody().getTextOffset() + 1, dto.getAddedText());
        }
        PsiDocumentUtils.commitAndSaveDocument(psiDocumentManager, document);

        if (dto.getImportList() != null) {
            PsiToolUtils.addImportToFile(psiDocumentManager, (PsiJavaFile) element.getContainingFile(), document, dto.getImportList());
        }
    }

    @NotNull
    private static InsertDto getBaseInsertDto(String splitText, PsiParameter[] parameters1, @NotNull PsiClass psiClass) {
        InsertDto dto = new InsertDto();
        PsiParameter[] parameters = parameters1;
        List<PsiMethod> methods = PsiClassUtils.extractSetMethods(psiClass);
        String generateName = PsiToolUtils.lowerStart(psiClass.getName());
        GetInfo info = null;
        if (parameters.length > 0) {
            for (PsiParameter parameter : parameters) {
                PsiType type = parameter.getType();
                PsiClass parameterClass = PsiTypesUtil.getPsiClass(type);
                if (parameterClass == null || parameterClass.getQualifiedName().startsWith("java.")) {
                    continue;
                } else {
                    List<PsiMethod> getMethods = PsiClassUtils.extractGetMethod(parameterClass);
                    // TODO: 2017/1/20 may be i can extract get memthod from all parameter
                    if (getMethods.size() > 0) {
                        info = buildInfo(parameter, getMethods);
                        break;
                    }
                }
            }
        }
        // TODO: 2017/8/2 what if two class has the same name
        String insertText = splitText + psiClass.getName() + " " + generateName + "= new " + psiClass.getName() + "();";
        if (info == null) {
            insertText += generateStringForNoParam(generateName, methods, splitText);
        } else {
            insertText += generateStringForParam(generateName, methods, splitText, info);
        }
        insertText += "return " + generateName + ";";
        dto.setAddedText(insertText);
        return dto;
    }

    @NotNull
    private static String generateStringForParam(String generateName, @NotNull List<PsiMethod> methodList, String splitText, @NotNull GetInfo info) {
        StringBuilder builder = new StringBuilder();
        builder.append(splitText);
        for (PsiMethod method : methodList) {
            if (method.getName().startsWith("get")) {
                String fieldToLower = method.getName().substring(3).toLowerCase();
                PsiMethod s = info.getNameToMethodMap().get(fieldToLower);
                if (s != null) {
                    // TODO: 2017/8/2 check the get method return type and set method param type.
//                    if (method.getParameterList().getParameters().length == 1) {
//                        PsiParameter psiParameter = method.getParameterList().getParameters()[0];
//                        PsiType type = psiParameter.getType();
//                        PsiType returnType = s.getReturnType();
//                        String setTypeText = type.getCanonicalText();
//                        String getTypeText = returnType.getCanonicalText();
//                        String getMethodText = info.getParamName() + "." + s.getName() + "()";
//                        String startText = generateName + "." + method.getName() + "(";
//                        builder.append(generateSetterString(setTypeText, getTypeText, getMethodText, startText));
//                    }
                } else {
//                    generateDefaultForOneMethod(generateName, builder, method);
                }
            }
            builder.append(splitText);
        }
        return builder.toString();
    }

    @NotNull
    private static String generateSetterString(@NotNull String setTypeText, @NotNull String getTypeText, String getMethodText, String startText) {
        if (setTypeText.equals(getTypeText)) {
            return startText + getMethodText + ");";
        } else {
            if (setTypeText.equals("java.lang.String")) {
                return startText + "String.valueOf(" + getMethodText + "));";
            } else if (setTypeText.equals("java.util.Date") && checkMethodIsLong(getTypeText)) {
                return startText + "new Date(" + getMethodText + "));";
            } else if (checkMethodIsLong(setTypeText) && getTypeText.equals("java.util.Date")) {
                return startText + getMethodText + ".getTime());";
            } else if (setTypeText.equals("java.sql.Timestamp") && checkMethodIsLong(getTypeText)) {
                return startText + "new Timestamp(" + getMethodText + "));";
            } else if (checkMethodIsLong(setTypeText) && getTypeText.equals("java.sql.Timestamp")) {
                return startText + getMethodText + ".getTime());";
            }
        }
        return startText + getMethodText + ");";
    }

    private static boolean checkMethodIsLong(@NotNull String getMethodText) {
        return getMethodText.equals("java.lang.Long") || getMethodText.equals("long");
    }

    @NotNull
    private static GetInfo buildInfo(@NotNull PsiParameter parameter, @NotNull List<PsiMethod> getMethods) {
        GetInfo info;
        info = new GetInfo();
        info.setParamName(parameter.getName());
        info.setGetMethods(getMethods);
        Map<String, PsiMethod> nameToMethodMaps = Maps.newHashMap();
        for (PsiMethod getMethod : getMethods) {
            if (getMethod.getName().startsWith(IS)) {
                nameToMethodMaps.put(getMethod.getName().substring(2).toLowerCase(), getMethod);
            } else if (getMethod.getName().startsWith(GET)) {
                nameToMethodMaps.put(getMethod.getName().substring(3).toLowerCase(), getMethod);
            }
        }
        info.setNameToMethodMap(nameToMethodMaps);
        return info;
    }

    @NotNull
    private static String extractSplitText(@NotNull PsiMethod method, @NotNull Document document) {
        int startOffset = method.getTextRange().getStartOffset();
        int lastLine = startOffset - 1;
        String text = document.getText(new TextRange(lastLine, lastLine + 1));
        boolean isTable = false;
        while (!text.equals("\n")) {
            if (text.equals('\t')) {
                isTable = true;
            }
            lastLine--;
            text = document.getText(new TextRange(lastLine, lastLine + 1));
        }
        String methodStartToLastLineText = document.getText(new TextRange(lastLine, startOffset));
        String splitText = null;
        if (isTable) {
            splitText += methodStartToLastLineText + "\t";
        } else {
            splitText = methodStartToLastLineText + "    ";
        }
        return splitText;
    }


    private void handleWithLocalVariable(@NotNull PsiLocalVariable localVariable, @NotNull Project project, @NotNull PsiElement element) {
        PsiElement parent1 = localVariable.getParent();
        if (!(parent1 instanceof PsiDeclarationStatement)) {
            return;
        }
        PsiClass psiClass = PsiTypesUtil.getPsiClass(localVariable.getType());
        String generateName = localVariable.getName();
        List<PsiMethod> methodList = PsiClassUtils.extractGetMethod(psiClass);
        if (methodList.size() == 0) {
            return;
        }
        PsiDocumentManager psiDocumentManager = PsiDocumentManager.getInstance(project);
        PsiFile containingFile = element.getContainingFile();
        Document document = psiDocumentManager.getDocument(containingFile);
        int statementOffset = parent1.getTextOffset();
        String splitText = "";
        int cur = statementOffset;
        String text = document.getText(new TextRange(cur - 1, cur));
        while (text.equals(" ") || text.equals("\t")) {
            splitText = text + splitText;
            cur--;
            if (cur < 1) {
                break;
            }
            text = document.getText(new TextRange(cur - 1, cur));
        }
        splitText = "\n" + splitText;


        Set<String> newImportList = new HashSet<>();
        boolean hasGuava = PsiToolUtils.checkGuavaExist(project, element);

        String buildString = generateStringForNoParam(generateName, methodList, splitText);
        document.insertString(statementOffset + parent1.getText().length(), buildString);
        PsiDocumentUtils.commitAndSaveDocument(psiDocumentManager, document);
        PsiToolUtils.addImportToFile(psiDocumentManager, (PsiJavaFile) containingFile, document, newImportList);
        return;
    }

    @NotNull
    private static String generateStringForNoParam(String generateName, @NotNull List<PsiMethod> methodList, String splitText) {
        StringBuilder builder = new StringBuilder();
        builder.append(splitText);
        for (PsiMethod method : methodList) {
            generateDefaultForOneMethod(generateName, builder, method);
            builder.append(splitText);
        }

        return builder.toString();
    }

    private static void generateDefaultForOneMethod(String generateName, @NotNull StringBuilder builder, @NotNull PsiMethod method) {
        String[] split = method.getReturnType().getCanonicalText().split("\\.");
        int length = split.length;
        String variableType = split[length - 1];
        String variableName;
        if ( method.getName().length() > 3) {
            variableName = method.getName().substring(3, 4).toLowerCase() + method.getName().substring(4);
        } else  {
            variableName = method.getName().substring(3, 4).toLowerCase();
        }
        builder.append(variableType + " " + variableName + "1 = " + generateName + "." + method.getName() + "();");
    }
    
    @Override
    public boolean isAvailable(@NotNull Project project, Editor editor, @NotNull PsiElement element) {
        PsiElement psiParent = PsiTreeUtil.getParentOfType(element, PsiLocalVariable.class, PsiMethod.class);
        if (psiParent == null) {
            return false;
        }

        PsiClass psiClass = null;
        if (psiParent instanceof PsiLocalVariable) {
            PsiLocalVariable psiLocal = (PsiLocalVariable) psiParent;
            if (!(psiLocal.getParent() instanceof PsiDeclarationStatement)) {
                return false;
            }
            psiClass = PsiTypesUtil.getPsiClass(psiLocal.getType());


        } else if (psiParent instanceof PsiMethod) {
            PsiMethod method = (PsiMethod) psiParent;
//            if is constructor the return type will be null.
            if (method.getReturnType() == null) {
                return false;
            }
            psiClass = PsiTypesUtil.getPsiClass(method.getReturnType());
            ParamInfo returnTypeInfo = PsiToolUtils.extractParamInfo(method.getReturnType());
            if (returnTypeInfo.getCollectPackege() != null && handlerMap.containsKey(returnTypeInfo.getCollectPackege())) {
                return true;
            }
        }
        //todo when psiClass is null, it can be list ect. can generate it as well could use method.getReturnType.getCanolicText to check for type ect.
        //todo may check with the cursor if it on the method definition area instead of everywhere in method.
        return psiClass != null && PsiClassUtils.checkClassHasValidGetMethod(psiClass);
    }

    private static boolean notObjectClass(@NotNull PsiClass psiClass) {
        if (psiClass.getQualifiedName().equals("java.lang.Object")) {
            return false;
        }
        return true;
    }

    @Nls
    @NotNull
    @Override
    public String getFamilyName() {
        return GENERATE_SETTER_METHOD;
    }

    @NotNull
    @Override
    public String getText() {
        return GENERATE_SETTER_METHOD;
    }
}
