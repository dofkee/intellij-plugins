package com.lucky.idea;

import org.jetbrains.annotations.NotNull;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * Author: lucky
 * Date: 28.02.2009 22:51:00
 * Powered by Jedi Force. Use da toilet, Luk!!!
 */
public class DirHash {
    public static final String SPLITTER = ",";
    public static final String ENC = "UTF-8";
    @NotNull
    private final List<String> list;
    @NotNull
    private final Map<String, ArrayList<Integer>> tagCache;
    @NotNull
    private final Map<String, ArrayList<String>> tagTitleCache;
    @NotNull
    private final Map<String, Integer> titleCache;

    public DirHash() {
        list = new ArrayList<>(0);
        tagCache = new HashMap<>(0);
        tagTitleCache = new HashMap<>(0);
        titleCache = new HashMap<>(0);
    }

    public void update(@NotNull String title, @NotNull String tags, String content) {
        String[] allTags = tags.split(SPLITTER);
        if (allTags.length == 0) {
            return;
        }
        update(title, allTags, content);
    }

    public void update(@NotNull String title, @NotNull String[] allTags, String content) {
        if (title.trim().isEmpty()) {
            title = "No title";
        }
        list.add(content);
        int indexOfContent = list.indexOf(content);
        if (!titleCache.containsKey(title)) {
            titleCache.put(title, indexOfContent);
        } else {
            int i = 2;
            while (true) {
                String tmp;
                tmp = title + " #" + i;
                if (!titleCache.containsKey(tmp)) {
                    title = tmp;
                    titleCache.put(title, indexOfContent);
                    break;
                }
                i++;
            }
        }
        for (String tag : allTags) {
            if (tag != null && !(tag = tag.trim()).isEmpty()) {
                tagCache.computeIfAbsent(tag, k -> new ArrayList<>(0));
                tagTitleCache.computeIfAbsent(tag, k -> new ArrayList<>(0));
                tagCache.get(tag).add(indexOfContent);
                tagTitleCache.get(tag).add(title);
            }
        }
    }

    public void update(@NotNull String title, @NotNull List<String> allTags, String content) {
        update(title, allTags.toArray(new String[0]), content);
    }

    @NotNull
    public List<String> searchContent(String part) {
        final List<String> results = new ArrayList<>(0);
        part = part.toLowerCase();
        for (String content : list) {
            if (content.toLowerCase().contains(part)) {
                final int index = list.indexOf(content);
                for (Map.Entry<String, Integer> entry : titleCache.entrySet()) {
                    if (entry.getValue() == index) {
                        results.add(entry.getKey());
                        break;
                    }
                }
            }
        }
        return results;
    }

    @NotNull
    public List<String> searchTitle(@NotNull String part) {
        String tmp = part.toLowerCase();
        final List<String> results = new ArrayList<>(0);
        for (String title : titleCache.keySet()) {
            if (title.toLowerCase().contains(tmp)) {
                results.add(title);
            }
        }
        return results;
    }

    @NotNull
    public List<String> searchTags(@NotNull String part) {
        String tmp = part.toLowerCase();
        final List<String> results = new ArrayList<>(0);
        for (String tag : tagCache.keySet()) {
            if (tag.toLowerCase().contains(tmp)) {
                results.add(tag);
            }
        }
        return results;
    }

    @NotNull
    public String toXml() throws UnsupportedEncodingException {
        StringBuilder xml = new StringBuilder();
        xml.append("<?xml version='1.0' encoding='"+ENC+"'?><hashdir>");
        for (String content : list) {
            if (content.isEmpty()) {
                continue;
            }
            final int index = list.indexOf(content);
            xml.append("<element>");
            for (Map.Entry<String, Integer> entry : titleCache.entrySet()) {
                if (entry.getValue() == index) {
                    xml.append("<title>").append(URLEncoder.encode(entry.getKey(), ENC)).append("</title>");
                    break;
                }
            }
            for (Map.Entry<String, ArrayList<Integer>> entry : tagCache.entrySet()) {
                if (entry.getValue().contains(index)) {
                    xml.append("<tag>").append(URLEncoder.encode(entry.getKey(), ENC)).append("</tag>");
                }
            }
            xml.append("<content>").append(URLEncoder.encode(content, ENC)).append("</content>");
            xml.append("</element>");
        }
        xml.append("</hashdir>");
        return xml.toString();
    }

    @NotNull
    public List<String> getTags() {
        return new ArrayList<>(tagCache.keySet());
    }

    public List<String> searchTitles(String tag) {
        return tagTitleCache.get(tag);
    }

    @NotNull
    public String getContentByTitle(String title) {
        Integer index = titleCache.get(title);
        return index != null ? list.get(index) : "";
    }

    @NotNull
    public Set<String> getTitles() {
        return titleCache.keySet();
    }

    public void dropByTitle(String title) {
        int index = titleCache.remove(title);
        list.set(index, "");
        for (List<String> strings : tagTitleCache.values()) {
            strings.remove(title);
        }
    }

    @NotNull
    public String getTagsByTitle(String title) {
        StringBuilder tags = new StringBuilder();
        for (Map.Entry<String, ArrayList<String>> entry : tagTitleCache.entrySet()) {
            if (entry.getValue().contains(title)) {
                tags.append(tags.length() > 0 ? ", " : "").append(entry.getKey());
            }
        }
        return tags.toString();
    }

    public void updateByTitle(String oldTitle, String newTitle, @NotNull String tags, String content) {
        final int index = titleCache.remove(oldTitle);
        list.set(index, content);
        titleCache.put(newTitle, index);
        for (List<String> strings : tagTitleCache.values()) {
            strings.remove(oldTitle);
        }
        for (List<Integer> integers : tagCache.values()) {
            if (integers.contains(index)) {
                integers.remove(Integer.valueOf(index));
            }
        }
        if (!tags.isEmpty()) {
            for (String tag : tags.split(SPLITTER)) {
                if (tag != null && !(tag = tag.trim()).isEmpty()) {
                    tagCache.computeIfAbsent(tag, k -> new ArrayList<>(0));
                    tagTitleCache.computeIfAbsent(tag, k -> new ArrayList<>(0));
                    tagCache.get(tag).add(index);
                    tagTitleCache.get(tag).add(newTitle);
                }
            }
        }
    }
}
