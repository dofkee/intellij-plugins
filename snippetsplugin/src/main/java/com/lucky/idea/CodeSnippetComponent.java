package com.lucky.idea;

import com.intellij.openapi.application.PathManager;
import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.ui.Messages;
import com.lucky.idea.utils.HashParser;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by IntelliJ IDEA.
 * Author: lucky
 * Date: 01.03.2009 1:17:27
 * Powered by Jedi Force. Use da toilet, Luk!!!
 */
public class CodeSnippetComponent implements ApplicationComponent {
    private DirHash hash;
    @NotNull
    final File backFile;

    public CodeSnippetComponent() {
        backFile = new File(new File(PathManager.getConfigPath()), "codesnippets.xml");
    }

    public DirHash getHash() {
        return hash;
    }

    public void initComponent() {
        try {
            hash = initHash();
        } catch (Exception e) {
            hash = new DirHash();
        }
    }

    @NotNull
    private DirHash initHash() throws Exception {
        if (!backFile.exists() && backFile.createNewFile()) {
            return new DirHash();
        }

        final DirHash tmp = new DirHash();

        XMLReader xmlReader = SAXParserFactory.newInstance().newSAXParser().getXMLReader();

        xmlReader.setContentHandler(new HashParser(tmp));

        xmlReader.parse(new InputSource(new FileInputStream(backFile)));

        return tmp;
    }

    public void disposeComponent() {
        doSave();
    }

    private void doSave() {
        try {
            try (OutputStream os = new FileOutputStream(backFile)) {
                final InputStream is = new ByteArrayInputStream(hash.toXml().getBytes("UTF-8"));

                final byte[] tmp = new byte[2048 * 32];
                int read;

                try {
                    while ((read = is.read(tmp, 0, tmp.length)) != -1) {
                        os.write(tmp, 0, read);
                    }

                    os.flush();
                } finally {
                    try {
                        os.close();
                    } catch (Exception ignore) {
                    }
                    try {
                        is.close();
                    } catch (Exception ignore) {
                    }
                }
            }

        } catch (Exception e) {
            Messages.showErrorDialog(e.getMessage(), "Error save");
        }
    }

    @NotNull
    public String getComponentName() {
        return "CodeSnippetComponent";
    }
}
