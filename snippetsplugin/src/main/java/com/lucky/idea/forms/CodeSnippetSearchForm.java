package com.lucky.idea.forms;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;

public class CodeSnippetSearchForm extends GenericEditorForm {
    //<editor-fold desc="fields">
    @NotNull private JList taglist;
    @NotNull private JList titlelist;
    @NotNull private JTextField tagsSearch;
    @NotNull private JPanel snippetSource;
    @NotNull private JTextField titleSearch;
    @NotNull private JTextField content;
    @NotNull private JButton applyButton;
    @NotNull private Document document;
    private int cursorOffset;
    @NotNull private JPanel mainPanel;
    @NotNull private final JFrame frame;
    //</editor-fold>

    public CodeSnippetSearchForm(@NotNull Project project, JFrame frame, @Nullable Editor editor) {
        super(project);
        this.frame = frame;

        updateTags(hash.getTags());
        if (editor != null) {
            this.cursorOffset = editor.getCaretModel().getOffset();
            this.document = editor.getDocument();
        }
        initListeners();

        fillEditorSettings(this.editor.getSettings());
        snippetSource.setLayout(new BorderLayout());
        snippetSource.add(this.editor.getComponent());
    }

    private void initListeners() {
        taglist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                resetForm(titleSearch, content, titlelist, editor);
                updateTitles(hash.searchTitles(getSelectedValue(taglist)));
            }
        });
        titlelist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                resetForm(content, editor);
                updateContent(hash.getContentByTitle(getSelectedValue(titlelist)));
            }
        });
        tagsSearch.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(@NotNull KeyEvent e) {
                resetForm(titleSearch, content, titlelist, editor);
                if (!(tagsSearch.getText() + e.getKeyChar()).isEmpty()) {
                    updateTags(hash.searchTags(tagsSearch.getText() + e.getKeyChar()));
                }
                if (tagsSearch.getText().isEmpty()) {
                    updateTags(hash.getTags());
                }
            }
        });
        titleSearch.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(@NotNull KeyEvent e) {
                resetForm(tagsSearch, taglist, content, editor);
                if (!(titleSearch.getText() + e.getKeyChar()).isEmpty()) {
                    updateTitles(hash.searchTitle(titleSearch.getText() + e.getKeyChar()));
                }
            }
        });
        content.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(@NotNull KeyEvent e) {
                resetForm(tagsSearch, taglist, titleSearch, editor);
                if (!(content.getText() + e.getKeyChar()).isEmpty()) {
                    updateTitles(hash.searchContent(content.getText() + e.getKeyChar()));
                }
            }
        });
        applyButton.addActionListener(action -> apply(action));

        mainPanel.registerKeyboardAction(e -> doExit(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

    }

    private void doExit() {
        frame.setVisible(false);
    }

    private void apply(@NotNull ActionEvent action) {
        final String text = editor.getDocument().getText();
        WriteCommandAction.writeCommandAction(null).run(() -> document.insertString(cursorOffset, text));
        cursorOffset = cursorOffset + text.length();
    }

    @NotNull
    String getSelectedValue(@NotNull JList taglist) {
        return taglist.getSelectedValue() != null ? taglist.getSelectedValue().toString() : EMPTY_STRING;
    }

    void resetForm(Object... objects) {
        final List<Object> list = Arrays.asList(objects);
        if (list.contains(tagsSearch)) {
            tagsSearch.setText(EMPTY_STRING);
        }
        if (list.contains(titleSearch)) {
            titleSearch.setText(EMPTY_STRING);
        }
        if (list.contains(content)) {
            content.setText(EMPTY_STRING);
        }
        if (list.contains(taglist)) {
            taglist.setModel(new DefaultListModel());
        }
        if (list.contains(titlelist)) {
            titlelist.setModel(new DefaultListModel());
        }
        if (list.contains(editor)) {
            updateContent(EMPTY_STRING);
        }
    }

    private void updateTags(@NotNull List<String> tags) {
        final DefaultListModel model = new DefaultListModel();
        Collections.sort(tags);
        tags.forEach(model::addElement);
        taglist.setModel(model);
        if (tags.size() == 1) {
            List<String> titles = hash.searchTitles(tags.get(0));
            updateTitles(titles != null ? titles : new ArrayList<>());
        }
    }

    private void updateContent(@NotNull String content) {
        if (!content.equals(editor.getDocument().getText())) {
            ApplicationManager.getApplication().runWriteAction(() -> editor.getDocument().setText(content));
        }
    }

    private void updateTitles(@NotNull final List<String> titles) {
        final DefaultListModel model = new DefaultListModel();
        Collections.sort(titles);
        titles.forEach(model::addElement);
        titlelist.setModel(model);
        if (titles.size() == 1) {
            updateContent(hash.getContentByTitle(titles.get(0)));
        }
    }

    @NotNull
    public Container getMainPanel() {
        return mainPanel;
    }
}
