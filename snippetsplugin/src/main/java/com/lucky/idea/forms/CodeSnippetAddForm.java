package com.lucky.idea.forms;

import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

public class CodeSnippetAddForm extends GenericEditorForm {

    //<editor-fold desc="fields">
    @NotNull private JTextField tags;
    @NotNull private JPanel mainPanel;
    @NotNull private JButton saveButton;
    @NotNull private JTextField title;
    @NotNull private JButton cancelBtn;
    @NotNull private JPanel content;
    @NotNull private final JFrame frame;
    //</editor-fold>

    public CodeSnippetAddForm(@NotNull JFrame frame, @NotNull Project project) {
        super(project);
        this.frame = frame;




        initListeners();


        fillEditorSettings(editor.getSettings());

        content.setLayout(new BorderLayout());
        content.add(editor.getComponent());
    }

    private void initListeners() {
        saveButton.addActionListener(e -> doSave());
        cancelBtn.addActionListener(e -> doExit());
        mainPanel.registerKeyboardAction(e -> doExit(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void doExit() {
        frame.setVisible(false);
    }

    private void doSave() {
        if (tags.getText().trim().isEmpty()) {
            Messages.showErrorDialog(messages.getString("tagMissing"), labels.getString("no.tags"));
            return;
        }
        if (editor.getDocument().getText().trim().isEmpty()) {
            Messages.showErrorDialog(messages.getString("emptySnippet"), messages.getString("no.content"));
            return;
        }

        hash.update(title.getText(), tags.getText(), editor.getDocument().getText());

        doExit();
    }

    @NotNull
    public Container getMainPanel() {
        return mainPanel;
    }
}
