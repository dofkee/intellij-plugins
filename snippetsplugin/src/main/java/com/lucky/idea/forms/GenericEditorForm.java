package com.lucky.idea.forms;

import com.intellij.lang.Language;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.EditorSettings;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileTypes.StdFileTypes;
import com.intellij.openapi.fileTypes.UnknownFileType;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDocumentManager;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiFileFactory;
import com.intellij.testFramework.LightVirtualFile;
import com.lucky.idea.CodeSnippetComponent;
import com.lucky.idea.DirHash;
import org.jetbrains.annotations.NotNull;

import java.util.ResourceBundle;

public class GenericEditorForm {
    public static final String EMPTY_STRING = "";
    @NotNull
    protected final DirHash hash;
    @NotNull
    protected final Editor editor;
    @NotNull
    protected ResourceBundle labels = ResourceBundle.getBundle("snippetLabels");
    @NotNull
    protected ResourceBundle messages = ResourceBundle.getBundle("snippetErrorMessages");

    public GenericEditorForm(Project project) {
        hash = ApplicationManager.getApplication().getComponent(CodeSnippetComponent.class).getHash();
        Document document = FileDocumentManager.getInstance().getDocument(new LightVirtualFile("DummyHolder", StdFileTypes.JAVA, ""));
        this.editor = EditorFactory.getInstance().createEditor(document, project, StdFileTypes.PLAIN_TEXT, false);
    }

    protected void fillEditorSettings(@NotNull final EditorSettings editorSettings) {
        editorSettings.setWhitespacesShown(true);
        editorSettings.setLineMarkerAreaShown(true);
        editorSettings.setIndentGuidesShown(true);
        editorSettings.setLineNumbersShown(true);
        editorSettings.setFoldingOutlineShown(false);
        editorSettings.setAdditionalColumnsCount(1);
        editorSettings.setAdditionalLinesCount(1);
        editorSettings.setUseSoftWraps(false);
    }

    @NotNull
    public Editor getEditor() {
        return editor;
    }
}
