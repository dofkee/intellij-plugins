package com.lucky.idea.forms;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;

public class CodeSnippetsManageForm extends GenericEditorForm {

    //<editor-fold desc="fields">
    @NotNull private final ArrayList<String> titles;
    @NotNull private JList titlelist;
    @NotNull private JButton updateBtn;
    @NotNull private JTextField titleSearch;
    @NotNull private JButton dropBtn;
    @NotNull private JButton closeBtn;
    @NotNull private JTextField tags;
    @NotNull private JTextField titleEdit;
    @NotNull private JPanel content;
    @NotNull private JPanel mainPanel;
    @NotNull private final JFrame frame;
    //</editor-fold>

    public CodeSnippetsManageForm(@NotNull JFrame frame, @NotNull Project project) {
        super(project);
        this.frame = frame;
        titles = new ArrayList<>(0);
        initListeners();
        fillEditorSettings(editor.getSettings());
        content.setLayout(new BorderLayout());
        content.add(editor.getComponent());
        init();
    }

    private void initListeners() {
        closeBtn.addActionListener(e -> doExit());
        updateBtn.addActionListener(e -> doUpdate());
        dropBtn.addActionListener(e -> doDrop());
        titlelist.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (titlelist.getSelectedIndex() >= 0) {
                    String title = getSelectedTitle();
                    updateContent(hash.getContentByTitle(title));
                    tags.setText(hash.getTagsByTitle(title));
                    titleEdit.setText(title);
                } else {
                    resetForm(true);
                }
            }
        });
        titleSearch.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(@NotNull KeyEvent e) {
                if (!(titleSearch.getText() + e.getKeyChar()).isEmpty()) {
                    titles.clear();
                    titles.addAll(hash.searchTitle(titleSearch.getText() + e.getKeyChar()));
                    updateTitles();
                }
                resetForm(false);
            }
        });

        mainPanel.registerKeyboardAction(e -> doExit(), KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void doExit() {
        frame.setVisible(false);
    }

    private void resetForm(final boolean withSearch) {
        if (withSearch) {
            titleSearch.setText(EMPTY_STRING);
        }
        titleEdit.setText(EMPTY_STRING);
        tags.setText(EMPTY_STRING);
        updateContent(EMPTY_STRING);
        updateBtn.setEnabled(false);
        dropBtn.setEnabled(false);
    }

    private void updateContent(@NotNull final String content) {
        if (!content.equals(editor.getDocument().getText())) {
            ApplicationManager.getApplication().runWriteAction(() -> {
                editor.getDocument().setText(content);
                updateBtn.setEnabled(true);
                dropBtn.setEnabled(true);
            });
        }
    }

    private void init() {
        titles.clear();
        titles.addAll(hash.getTitles());
        resetForm(true);
        updateTitles();
    }

    private void updateTitles() {
        final DefaultListModel model = new DefaultListModel();
        Collections.sort(titles);
        titles.forEach(model::addElement);
        titlelist.setModel(model);
        if (titles.size() == 1) {
            String firstTitle = titles.get(0);
            updateContent(hash.getContentByTitle(firstTitle));
            titleEdit.setText(firstTitle);
            tags.setText(hash.getTagsByTitle(firstTitle));
            titlelist.setSelectedIndex(0);
        }
    }

    private void doUpdate() {
        final String title = titleEdit.getText();
        final String tags = this.tags.getText();
        final String content = editor.getDocument().getText();
        if (content.trim().isEmpty()) {
            if (JOptionPane.showConfirmDialog(null, messages.getString("deleteEmptySnippet"), labels.getString("empty.content"), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                hash.dropByTitle(getSelectedTitle());
                init();
                return;
            }
        } else {
            if (tags.isEmpty() && JOptionPane.showConfirmDialog(null,
                    messages.getString("saveNoTags"), messages.getString("empty.tags"), JOptionPane.YES_NO_OPTION) == JOptionPane.NO_OPTION) {
                return;
            }
            if (title.isEmpty()) {
                JOptionPane.showMessageDialog(null, messages.getString("emptyTitle"), "No way",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        ApplicationManager.getApplication().runWriteAction(() -> {
            hash.updateByTitle(getSelectedTitle(), title, tags, content);
            init();
        });
    }

    @NotNull
    private String getSelectedTitle() {
        return titlelist.getSelectedValue() != null ? titlelist.getSelectedValue().toString() : EMPTY_STRING;
    }

    private void doDrop() {
        if (JOptionPane.showConfirmDialog(null, messages.getString("deleteSnippet"), labels.getString("drop.snippet"), JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            hash.dropByTitle(getSelectedTitle());
            init();
        }
    }

    @NotNull
    public Container getMainPanel() {
        return mainPanel;
    }
}
