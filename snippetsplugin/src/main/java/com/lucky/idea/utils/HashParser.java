package com.lucky.idea.utils;

import com.lucky.idea.DirHash;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * Author: lucky
 * Date: 01.03.2009 0:59:15
 * Powered by Jedi Force. Use da toilet, Luk!!!
 */
public class HashParser implements ContentHandler {

    public static final String UTF_8 = "UTF-8";
    public static final String EMPTY_STRING = "";

    private String innerString;

    private ElementDisp element;
    private final DirHash hash;

    public HashParser(DirHash hash) {
        innerString = EMPTY_STRING;
        this.hash = hash;
    }


    public void endDocument() { }

    public void startElement(String uri, String localName, @NotNull String qName, Attributes atts) {
        if (qName.equals("element")) {
            element = new ElementDisp();
        }
    }

    public void endElement(String uri, String localName, @NotNull String qName) {
        if (qName.equals("tag")) {
            try { element.tags.add(URLDecoder.decode(innerString, UTF_8)); } catch (UnsupportedEncodingException ignore) { }
        } else if (qName.equals("title")) {
            try { element.title = URLDecoder.decode(innerString, UTF_8); } catch (UnsupportedEncodingException ignore) { }
        } else if (qName.equals("content")) {
            try { element.content = URLDecoder.decode(innerString, UTF_8); } catch (UnsupportedEncodingException ignore) { }
        } else if (qName.equals("element")) {
            hash.update(element.title, element.tags, element.content);
        }

        innerString = EMPTY_STRING;
    }

    private class ElementDisp {
        ArrayList<String> tags;
        String content, title;

        ElementDisp() {
            tags = new ArrayList<>(0);
        }
    }

    public void characters(@NotNull char[] ch, int start, int length) {
        innerString += new String(ch, start, length);
    }

    public void startDocument() { }
    public void setDocumentLocator(Locator locator) { }
    public void startPrefixMapping(String prefix, String uri) { }
    public void endPrefixMapping(String prefix) { }
    public void ignorableWhitespace(char[] ch, int start, int length) { }
    public void processingInstruction(String target, String data) { }
    public void skippedEntity(String name) { }
}

