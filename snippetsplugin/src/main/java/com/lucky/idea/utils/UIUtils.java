package com.lucky.idea.utils;

import org.jetbrains.annotations.NotNull;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * Author: lucky
 * Date: 01.03.2009 2:07:18
 * Powered by Jedi Force. Use da toilet, Luk!!!
 */
public class UIUtils {
    public static void centerDialog(@NotNull Window frame) {
        final Dimension dialogSize = frame.getSize();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        frame.setLocation(screenSize.width/2 - dialogSize.width/2, screenSize.height/2 - dialogSize.height/2);
    }

    public static void placeUnderMouse(@NotNull Window frame) {
        frame.setLocation(MouseInfo.getPointerInfo().getLocation());
    }
}
