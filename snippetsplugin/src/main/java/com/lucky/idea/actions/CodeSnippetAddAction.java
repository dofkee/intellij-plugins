package com.lucky.idea.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.editor.Caret;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.impl.CaretImpl;
import com.intellij.openapi.editor.impl.EditorImpl;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.lucky.idea.forms.CodeSnippetAddForm;
import com.lucky.idea.utils.UIUtils;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.ResourceBundle;

public class CodeSnippetAddAction extends AnAction {

    ResourceBundle labels = ResourceBundle.getBundle("snippetLabels");

    public void actionPerformed(@NotNull AnActionEvent e) {
        final JFrame frame = new JFrame(labels.getString("snippet.add"));
        final Project project = e.getProject();
        CodeSnippetAddForm codeSnippetAddForm = new CodeSnippetAddForm(frame, project);

        frame.setContentPane(codeSnippetAddForm.getMainPanel());
        frame.setSize(862, 500);
        frame.setResizable(true);
        UIUtils.centerDialog(frame);
        frame.setVisible(true);

        if (project != null) {
            Editor textEditor = FileEditorManager.getInstance(project).getSelectedTextEditor();
            if (textEditor != null && textEditor.getSelectionModel().hasSelection()) {
                Editor selectedTextEditor = FileEditorManager.getInstance(project).getSelectedTextEditor();
                Caret primaryCaret = selectedTextEditor.getCaretModel().getPrimaryCaret();
                int start = primaryCaret.getSelectionStart();
                int end = primaryCaret.getSelectionEnd();
                String selectedText = selectedTextEditor.getDocument().getText(new TextRange(start, end));
                selectedText = selectedText + "\n//Snippet from: " + ((EditorImpl) selectedTextEditor).getVirtualFile().getPath();
                selectedText = selectedText + "#"+  (primaryCaret.getSelectionStartPosition().line+1)  + ":"+ (primaryCaret.getSelectionEndPosition().line+1);

                codeSnippetAddForm.getEditor().getDocument().setText(selectedText);
            }
        }
    }
}
