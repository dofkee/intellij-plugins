package com.lucky.idea.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.lucky.idea.forms.CodeSnippetsManageForm;
import com.lucky.idea.utils.UIUtils;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.sql.CallableStatement;
import java.util.ResourceBundle;

public class CodeSnippetManageAction extends AnAction {
    private ResourceBundle resourceBundle = ResourceBundle.getBundle("snippetLabels");

    public void actionPerformed(@NotNull AnActionEvent e) {
        final JFrame frame = new JFrame(resourceBundle.getString("snippet.manage"));
        frame.setContentPane(new CodeSnippetsManageForm(frame, e.getProject()).getMainPanel());

        frame.setSize(770, 500);
        frame.setResizable(true);
        UIUtils.centerDialog(frame);

        frame.setVisible(true);
    }
}
