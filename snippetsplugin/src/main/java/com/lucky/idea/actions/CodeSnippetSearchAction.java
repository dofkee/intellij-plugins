package com.lucky.idea.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.lucky.idea.forms.CodeSnippetSearchForm;
import com.lucky.idea.utils.UIUtils;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.ResourceBundle;

public class CodeSnippetSearchAction extends AnAction {

    private ResourceBundle resourceBundle = ResourceBundle.getBundle("snippetLabels");

    public void actionPerformed(@NotNull AnActionEvent e) {
        final JFrame frame = new JFrame(resourceBundle.getString("snippet.search"));

        Editor selectedTextEditor = FileEditorManager.getInstance(e.getProject()).getSelectedTextEditor();
        frame.setContentPane(new CodeSnippetSearchForm(e.getProject(), frame ,selectedTextEditor).getMainPanel());
        frame.setSize(1000, 500);
        frame.setResizable(true);
        UIUtils.centerDialog(frame);

        frame.setVisible(true);
    }
}
